# Guide d'utilisation

## I. Sélection d'un territoire

À tout moment dans votre navigation, il est possible de sélectionner le territoire pour lequel vous souhaitez afficher les données de l'application en utilisant la liste déroulante présente en haut de la colonne de gauche :

![](www/screen_select.png)

Pour revenir sur l'emprise régionale, sélectionnez de nouveau la région dans cette même liste. 

## II. Cartes

### A) Afficher une étiquette

Il est possible d'afficher une étiquette pour chaque objet des différents cartes de l'application en cliquant :
- sur le symbole :

![](www/screen_map_01.png)

- au centre du cercle dans le cas d'une analyse thématique avec des cercles proportionnels :

![](www/screen_map_02.png)

### B) Modifier l'affichage de la carte 

Il est possible :
- de zoomer sur l'emprise d'une carte en cliquant sur le bouton "+" en haut à gauche de chaque carte
- de dézoomer sur l'emprise d'une carte en cliquant sur le bouton "-"
- d'afficher la carte en plein écran en cliquant sur le bouton avec un carré tireté :

![](www/screen_map_03.png)

Un clic sur le même bouton (avec un carré au milieu) ou un appui sur la touche F11 de votre clavier permet de revenir à l'état initial de la carte.

## III. Listes

### A) Pagination

Le tableau est paramétré pour afficher 20 lignes par défaut. Il est possible de modifier ce paramètre via une liste déroulante au dessus du tableau :

![](www/screen_list_01.png)

### B) Recherche

Une zone de recherche plein texte est dispsonible en haut à droite du tableau. 

La saisie d'une chaîne de caractères filtre les lignes sur tous les champs "texte" du tableau :

![](www/screen_list_03.png)

### C) Filtres

Des filtres sont accessibles pour certains champs sous les entêtes de colonnes :

Les champs suivants sont filtrables grâce à une liste déroulante des différentes valeurs :
* Département (uniquement si la région est le territoire sélectionné)
* Statut de la carrière
* Code de l'exploitant
* UD gestionnaire
* Code APE

![](www/screen_list_02.png)

Les champs suivants peuvent être filtrés via une réglette paramétrant une plage de valeur :
* Date de début d'autorisation
* Date de fin d'autorisation
* Production maximale autorisée

![](www/screen_list_05.png)

### D) Ascenseur horizontal

En fonction de la la résolution de votre écran, certaines colonnes du tableau peuvent ne pas être affichées par défaut.

Un ascenceur horizontal situé en bas du tableau permet de visualiser ces colonnes :

![](www/screen_list_04.png)

## IV. Graphiques

### A) Mettre une série en subrillance

Il suffit de positionner la souris sur une série dans la légende ou sur le graphique pour griser toutes les autres séries et donc mieux la visualiser :

![](www/screen_graph_01.png)

### B) Masquer/afficher une série

De la même façon, il suffit de cliquer sur une série dans la légende pour la masquer dans le graphique :

![](www/screen_graph_02.png)

## C) Utiliser les fonctions d'export

Sur chaque graphique, vous trouverez un menu latéral en haut à droite qui permet d'accéder aux fonctions d'export notamment :

![](www/screen_graph_03.png)

Ce menu offre les fonctionnalités suivantes :
- visualiser le graphique en plein écran,
- imprimer le graphique,
- télécharger le graphique dans divers formats d'image,
- télécharger les données du graphique dans un tableau dans différents formats.
