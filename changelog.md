# Journal des modifications

## Version 1.1.4

### Données

- Regroupements d'items pour les types de substances

### Fonctionnalités implémentées
Graphiques de répartition de la production :
- Modification des titres
- Ajout des sources
- Ajout du nom du département

### Bugs résolus

**Statistiques :**
- Zoom mal réglé sur la carte en page d'accueil pour les écrans de petite taille
- Pas d'affichage des graphiques de production par département
- Pas d'affichage des pourcentages sur les graphiques de production

## Version 1.1.3

### Fonctionnalités implémentées

**Page d'accueil :** Affichage du millésime des données

**Statistiques :**
- Ajout de graphiques de répartition de la production en pourcentage
- Ajout d'une catégorie "Type de substance"
- Automatisation de l'affichage des séries dans la production des graphiques d'évolution

**Cartes :** Ajout des limites départementales sur les cartes régionales

### Données

- Intégration du millésime 2020 pour la source GEREP
- Mise à jour des données S3IC :  correction d'erreurs
- Mise à jour de la table r_tableau_carriere_r52 : correction d'erreurs

## Version 1.1.2

### Données

- Problème de triplon pour une carrière
- Problème de décalage du nombre de carrières en 44
- Problème de décalage du nombre de carrières en 49

### Bugs résolus

**Statistiques :**
- Problème d'affichage des graphiques d'évolution de la production par département
- Affichage erroné de l'année de production
- Problème d'affichage des cercles proportionnels sur la carte des productions
- Problème de correspondance pour les types de ressources dans la rubrique "Graphiques et tableaux"

## Version 1.1.1

### Fonctionnalités implémentées

- Ajout de la date d'actualisation exacte des données
- Modification du nom de l'établissement

### Données

- Mise à jour des données à partir des sources S3IC et GEREP
- Remplacement de UD 49, 72 et 53 par UIDAM
- Mise à jour de la table r_tableau_carriere_r52
- Mise à jour des tables de jointure

### Bugs résolus

- Problème d'affichage du graphique par type de ressource par département
- Problème d'affichage d'une carrière dans les cartes
- Problème d'affichage des carrières avec plusieurs types de ressources dans les cartes
- Problème de filtre sur la liste des carrières

### Documentation

- Mise à jour du glossaire
- Ajout d'une aide pour la pagination des résultats

## Version 1.1

### Fonctionnalités implémentées

**Page d'accueil :** Affichage de la date d'export des données

**Liste des carrières :** Ajout d'un ascenseur horizontal au tableau

**Cartes :**
- Ajout du nom de la commune aux étiquettes
- Ajout d'une option d'affichage plein écran
- Ajout des sources

### Bugs résolus

- Affichage de la page d'accueil sur certains écrans
- Problème d'encodage de caractères 
- Harmonisation du nom de l'établissement entre les différentes cartes
- Correction de l'absence de roches métamorphiques dans les analyses par type de roche
- Ajout de catégories pour les analyses par type de carrière
- Affichage de la série "Autres matériaux" sur le graphique Évolution de la production par type de carrière
- Correction d'un problème sur la carte des carrières en fonctionnement

## Version 1

### Fonctionnalités implémentées

- Ajout d'une sélection par département via une liste déroulante dans la colonne de gauche qui génère un affichage par département pour les éléments suivants :
 - titres des rubriques "Liste des carrières", "Statistiques", "Cartes"
 - carte des carrières en fonctionnement sur la page d'accueil
 - chiffres-clés par département
 - sélection de la liste des carrières
 - graphiques et tableaux
 - évolution de la production par département
 - cartes avec analyses thématiques
- Rappel de la source et de la date des données dans les graphiques
- Enrichissement de l'affichage des étiquettes

### Bugs résolus

- Suppression de la colonne "Département" dans la liste des carrières si un département est sélectionné
- Revoir l'affichage des analyses thématiques
- Problème d'affichage de la carte des productions
- Problème d'affichage des étiquettes sur les cartes des analyses thématiques
- Remplacement des valeurs par défaut pour les dates nulles et les productions vides

----------
